/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ArbolBinario;
import ufps.util.colecciones_seed.NodoBin;
import ufps.util.graficador.BTreePrinter;

/**
 *
 * @author madar
 */
public class TestArbolBinario {
    
    public static void main(String[] args) {
        ArbolBinario<Integer> myArbol=new ArbolBinario();
        NodoBin<Integer> n1=new NodoBin(5);
        NodoBin<Integer> n2=new NodoBin(1);
        NodoBin<Integer> n3=new NodoBin(6);
        NodoBin<Integer> n4=new NodoBin(2);
        NodoBin<Integer> n5=new NodoBin(4);
        NodoBin<Integer> n6=new NodoBin(9);
        NodoBin<Integer> n7=new NodoBin(8);
        
        //Armar el árbol:
        //1. Insertarmos la raíz:
        myArbol.setRaiz(n1);
        //Nodos Ramas y Hojas
        n1.setIzq(n2);
        n1.setDer(n3);
        n3.setIzq(n4);
        n3.setDer(n5);
        n2.setIzq(n6);
        n2.setDer(n7);
        BTreePrinter.printNode(myArbol.getRaiz());
        
        System.out.println("El árbol tiene :"+myArbol.getCantidadHojas()+" hojas");
        System.out.println("El árbol tiene las siguientes hojas:"+myArbol.getHojas());
        System.out.println("El árbol tiene las siguientes nodos ramas:"+myArbol.getInfoNodosRamas());
        System.out.println("El árbol tiene las siguientes hojas derechas:"+myArbol.getInfoHojasDerechas());
        System.out.println("El árbol tiene el siguiente código Luka:"+myArbol.getLukasiewicz());
        
        System.out.println("El árbol tiene el siguiente recorrido PreOrden:"+myArbol.getPreorden());
        System.out.println("El árbol tiene el siguiente recorrido InOrden:"+myArbol.getInorden());
        System.out.println("El árbol tiene el siguiente recorrido PostOrden:"+myArbol.getPostorden());
        System.out.println("El árbol tiene el siguiente recorrido PreOrden Iterativo:"+myArbol.getPredorden_Iterativo());
        System.out.println("El árbol tiene el siguiente recorrido InOrden Iterativo:"+myArbol.getIndorden_Iterativo());
        System.out.println("El árbol tiene los siguientes elementos por niveles:"+myArbol.getElementos_Nivel());
        System.out.println("Está el elemento 6:"+myArbol.estaElemento(6));
        System.out.println("Está el elemento 10:"+myArbol.estaElemento(10));
        System.out.println("Descripción: " + myArbol.getDescripcionElemento(15));
    }
    
}
